#!/usr/bin/env python

"""
Number guesser between 1 and 100.
"""
import random

def main():
    username = raw_input("Hello, what is your name? ")
    print "Well " + username + ", guess a number between 1 and 100."

    randomnumber = random.randint(1,100)
    found = False
    attempts = 1

    while not found:
        userguess = input("Your guess: ")

        if attempts == 6:
            print "You are not too good at this game.."

        if userguess == randomnumber:
            print "You guessed correctly in " + str(attempts) + " attempts !"
            found = True
        elif userguess > randomnumber:
            print "Guess lower.."
        else:
            print "Guess higher.."
        attempts += 1

    print "Thanks for playing " + username + " !"


if __name__ == "__main__":
    main()


